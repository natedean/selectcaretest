import PropTypes from 'prop-types';
import React from 'react';

/**
 * @function Col
 * @param {string} [props.xs] - xs column width
 * @param {string} [props.sm] - sm column width
 * @param {string} [props.md] - md column width
 * @param {string} [props.lg] - lg column width
 * @param {string} [props.className] - The classname(s)
 * @param {function} [props.onClick] - The function for onClick handler
 * @param {HTMLElement} [props.children] - Child node(s)
 * @description
 * Returns div with flex grid properties that are passed to it.
 * @example
 * <Col xs="12" sm="6" md="4" lg="3" className="someClassName"></Col>
 * @return {object} - jsx
 */
const Col = ({xs, sm, md, lg, className = '', children, onClick}) => {
	const extraSmall = xs ? `col-xs-${xs} ` : '';
	const small = sm ? `col-sm-${sm} ` : '';
	const medium = md ? `col-md-${md} ` : '';
	const large = lg ? `col-lg-${lg} ` : '';

	return (
		<div onClick={onClick} className={extraSmall + small + medium + large + className}>{children}</div>
	);
};

Col.propTypes = {
	children: PropTypes.node,
	className: PropTypes.string,
	xs: PropTypes.string,
	sm: PropTypes.string,
	md: PropTypes.string,
	lg: PropTypes.string,
	onClick: PropTypes.func
};

export default Col;
