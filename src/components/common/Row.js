import PropTypes from 'prop-types';
import React from 'react';

/**
 * @function Row
 * @param {string} [props.className] - The classname(s)
 * @param {function} [props.onClick] - The function for onClick handler
 * @param {HTMLElement} [props.children] - Child node(s)
 * @description
 * Returns a div with the class row that uses the flex grid layout
 * @example
 * <Row className="someClassName"></Row>
 * @return {object} - jsx
 */
const Row = ({className = '', children, onClick}) => {
	return <div onClick={onClick} className={`row ${className}`}>{children}</div>;
};

Row.propTypes = {
	children: PropTypes.node,
	className: PropTypes.string,
	onClick: PropTypes.func
};

export default Row;
