import PropTypes from 'prop-types';
import React from 'react';
import Paper from 'material-ui/Paper';
import {menuRoutes as routes} from '../../routes';
import HomeMenu from '@sqlabs/the-shell-web/src/components/HomeMenu/HomeMenu';

/**
 * This is now just a stateless function.
 *
 *  -- If you do not need access to state then this approach has a couple advantages --
 *  1.) You don't need to bother extending the React.Component class which means lower memory footprint
 *  2.) This is now easier to test, you can only pass props in and display components, limits the potential for failure
 *      since there is no lifecycle.
 */

/**
 * @function Home
 * @description
 * Entry page to the app
 * @return {object} - jsx
 */
const Home = () => {
	return (
		<Paper style={{ padding: '1rem' }}>
			<div className="domainHome">
				<HomeMenu routes={routes}/>
			</div>
		</Paper>
	);
};

Home.propTypes = {
	rowBackgroundColor: PropTypes.string
};

export default Home;
