import LazyFinance from './LazyFinance';
import LazyMarketing from './LazyMarketing';
import LazySales from './LazySales';

export {
	LazyFinance,
	LazyMarketing,
	LazySales
};
