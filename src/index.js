import React, {Component} from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {createRoutes} from './routes';
import configureStore from '@sqlabs/the-shell-web/src/configureStore';
import createRouter from '@sqlabs/the-shell-web/src/services/createRouter';

import './styles/scss/styles.scss';

//Variables
const app = document.getElementById('app');
const store = configureStore({});
const Router = createRouter(createRoutes(store));

// import {syncHistoryWithStore} from 'react-router-redux';
// Create an enhanced history that syncs navigation events with the store

render(
	<Provider store={store}>
		<Router />
	</Provider>
	, app
);
