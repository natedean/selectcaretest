const initialState = {
	isNotifying: false,
	notificationMessage : 'You\'ve just been notified!!',
	externalExamplesLink: 'http://redux.js.org/docs/introduction/Examples.html',
	notificationCount: 0
};

export default initialState;
