import initialState from './initialAppState';
import Types from '../actions/actionTypes';

const notificationMessage = (state = initialState.notificationMessage, action) => {
	switch(action.type) {
		case Types.CHANGE_NOTIFICATION_MESSAGE:
			return action.value;
		default:
			return state;
	}
};

export default notificationMessage;
