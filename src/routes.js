// import SalesRoutes from 'sales';
// import MarketingRoutes from 'marketing';

import { createRoutes as createFinanceRoutes } from '@sqlabs/FinanceTest/src/routes';
import { createRoutes as createSalesRoutes } from '@sqlabs/SalesTest/src/routes';
import {createMenu} from '@sqlabs/the-shell-web/src/services/menuHelpers';
import Home from './components/home/Home';
import React from 'react';
import Marketing from './components/common/Marketing';
import Login from './components/common/Login';

export const createRoutes = (store) => [
	{path:'/', name:'SelectCare', component: Home},
	...createSalesRoutes(store),
	...createFinanceRoutes(store),
	{path:'/marketing', name:'Marketing Domain', component: Marketing},
	{path:'/login', name:'Login', component: Login}
];

export const menuRoutes = createMenu(createRoutes({}));
