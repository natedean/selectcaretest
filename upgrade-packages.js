const child_process = require('child_process');
const { exec, execSync } = child_process;
const scope = process.argv[2] || '@sqlabs'; // default to '@sqlabs', since this is our main use case

exec(`echo upgrading ${scope} packages`, start);

function start() {
	log(...arguments);

	Object.keys(require('./package.json').dependencies)
		.filter(key => key.startsWith(scope))
		.forEach(packageName => {
			execSync(`yarn upgrade ${packageName}`);
		});

	exec(`echo all ${scope} packages upgraded`, log);
}

function log(err, stdout, stderr) {
	console.log(err || stdout || stderr);
}
