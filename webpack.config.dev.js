import webpack from 'webpack';
import path from 'path';
import autoprefixer from 'autoprefixer';
import jsonImporter from 'node-sass-json-importer';

const GLOBALS = {
	'process.env.NODE_ENV': process.env.NODE_ENV && JSON.stringify(process.env.NODE_ENV),
	'process.env.PORT': process.env.PORT && JSON.stringify(process.env.PORT),
	'process.env.SOCKET_SERVER': process.env.SOCKET_SERVER && JSON.stringify(process.env.SOCKET_SERVER)
};

export default {
	debug: true,
	devtool: 'source-map',
	noInfo: false,
	entry: [
		'babel-polyfill',
		'eventsource-polyfill', // necessary for hot reloading with IE
		'webpack-hot-middleware/client?reload=true', //note that it reloads the page if hot module reloading fails.
		'whatwg-fetch',
		'./src/index'
	],
	target: 'web',
	output: {
		path: __dirname + '/dist', // Note: Physical files are only output by the production build task `npm run build`.
		publicPath: '/',
		filename: 'bundle.js'
	},
	devServer: {
		contentBase: './src'
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.DefinePlugin(GLOBALS),
		new webpack.NoErrorsPlugin()
	],
	module: {
		loaders: [
			{test: /\.js$/, include: [path.join(__dirname, 'src'), path.join(__dirname, 'node_modules/@sqlabs')], loaders: ['babel']},
			{test: /\.css$/, loader: "style!css"},
			{test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
			{test: /\.(png|jpe?g|gif|ttf|woff|woff2)$/, loader: 'url'},
			{test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'},
			{ test: /\.scss$/, loaders: ['style', 'css?sourceMap', 'postcss', 'sass?sourceMap']}
		]
	},
	sassLoader: {
		importer: jsonImporter
	},
	postcss: [autoprefixer({browsers: ['last 3 versions']})]
};
